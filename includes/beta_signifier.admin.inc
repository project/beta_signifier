<?php

/**
 * @file
 * Administrative interface to configure Beta Signifier.
 */

/**
 * Settings form.
 */
function beta_signifier_settings_form() {
  $form['form_element'] = array(
    '#type' => 'fieldset',
    '#title' => t('Beta Signifier on form elements'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
    '#description' => t("The Beta Signifier will show up on form elements where #beta is set to TRUE using the <a href=\"https://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7\">Form API</a>, for instance:<pre>\$form['myelement'] = array(\n  '#type' => 'textfield',\n  '#title' => t('My Element'),\n  '#beta' => TRUE,\n);</pre>"),
  );
  $form['form_element']['beta_signifier_form_element_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Message'),
    '#required' => TRUE,
    '#default_value' => variable_get('beta_signifier_form_element_message', 'This feature is in beta test mode'),
    '#description' => t('Content of the Beta Signifier when enabled on a form element.'),
  );
  $form['form_element']['beta_signifier_form_element_color_background_picker'] = array(
    '#markup' => '<div id="beta-signifier-form-element-color-background-picker"></div>',
  );
  $form['form_element']['beta_signifier_form_element_color_background'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color'),
    '#description' => t('Background color for the signifier. Ex: #008000'),
    '#default_value' => variable_get('beta_signifier_form_element_color_background', '#008000'),
    '#required' => TRUE,
    '#attached' => array(
      'library' => array(
        array('system', 'farbtastic'),
      ),
    ),
  );
  $form['form_element']['beta_signifier_form_element_color_text_picker'] = array(
    '#markup' => '<div id="beta-signifier-form-element-color-text-picker"></div>',
  );
  $form['form_element']['beta_signifier_form_element_color_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Text color'),
    '#description' => t('Text color for the signifier. Ex: #ffffff'),
    '#default_value' => variable_get('beta_signifier_form_element_color_text', '#ffffff'),
    '#required' => TRUE,
    '#attached' => array(
      'library' => array(
        array('system', 'farbtastic'),
      ),
    ),
  );
  $form['page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Beta Signifier on pages'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );
  $form['page']['beta_signifier_page_list'] = array(
    '#type' => 'textarea',
    '#title' => t('Enable on these pages'),
    '#default_value' => variable_get('beta_signifier_page_list', ''),
    '#description' => t('Enter one page per line as Drupal paths. The \'*\' character is a wildcard. Example paths are \'blog\' for the blog page and \'blog/*\' for every personal blog. \'<front>\' is the front page.'),
  );
  $form['page']['beta_signifier_page_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Message'),
    '#required' => TRUE,
    '#default_value' => variable_get('beta_signifier_page_message', 'This page is in beta test mode'),
    '#description' => t('Content of the Beta Signifier when enabled on a page.'),
  );
  $form['page']['beta_signifier_page_color_background_picker'] = array(
    '#markup' => '<div id="beta-signifier-page-color-background-picker"></div>',
  );
  $form['page']['beta_signifier_page_color_background'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color'),
    '#description' => t('Background color for the signifier. Ex: #ffffff'),
    '#default_value' => variable_get('beta_signifier_page_color_background', '#ffffff'),
    '#required' => TRUE,
    '#attached' => array(
      'library' => array(
        array('system', 'farbtastic'),
      ),
    ),
  );
  $form['page']['beta_signifier_page_color_text_picker'] = array(
    '#markup' => '<div id="beta-signifier-page-color-text-picker"></div>',
  );
  $form['page']['beta_signifier_page_color_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Text color'),
    '#description' => t('Text color for the signifier. Ex: #008000'),
    '#default_value' => variable_get('beta_signifier_page_color_text', '#008000'),
    '#required' => TRUE,
    '#attached' => array(
      'library' => array(
        array('system', 'farbtastic'),
      ),
    ),
  );
  $form['page']['beta_signifier_page_position'] = array(
    '#type' => 'radios',
    '#title' => t('Position'),
    '#options' => array(
      'page_top' => t('Top'),
      'page_bottom' => t('Bottom'),
    ),
    '#default_value' => variable_get('beta_signifier_page_position', 'page_top'),
    '#description' => t('Whether you want the Beta Signifier at the top or at the bottom of the page.'),
  );
  return system_settings_form($form);
}

