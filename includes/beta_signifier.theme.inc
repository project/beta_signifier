<?php

/**
 * @file
 * Theming the Beta Signifier.
 */

/**
 * Theme form element signifier.
 */
function theme_beta_signifier_form_element_signifier($variables) {
  $output = array(
    '#type' => 'html_tag',
    '#tag' => 'span',
    '#attributes' => array(
      'class' => array('beta-signifier-form-element-signifier'),
      'style' => 'background-color:' . check_plain($variables['color_background']) . ';color:' . check_plain($variables['color_text']),
    ),
    '#value' => check_plain($variables['text']),
  );
  return drupal_render($output);
}

/**
 * Theme page signifier.
 */
function theme_beta_signifier_page_signifier($variables) {
  $output = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('beta-signifier-page-signifier'),
      'style' => 'background-color:' . check_plain($variables['color_background']) . ';color:' . check_plain($variables['color_text']),
    ),
    'text' => array(
      '#theme' => 'html_tag',
      '#tag' => 'div',
      '#value' => check_plain($variables['text']),
    ),
  );
  return drupal_render($output);
}
