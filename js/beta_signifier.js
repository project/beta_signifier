(function ($) {
  /**
   * Add colorpickers.
   */
  Drupal.behaviors.betaSignifier = {
    attach: function(context, settings) {
      if ($.isFunction($.farbtastic)) {
        if ($('#beta-signifier-form-element-color-background-picker').length > 0) {
          $('#beta-signifier-form-element-color-background-picker').farbtastic('#edit-beta-signifier-form-element-color-background');
        }
        if ($('#beta-signifier-form-element-color-text-picker').length > 0) {
          $('#beta-signifier-form-element-color-text-picker').farbtastic('#edit-beta-signifier-form-element-color-text');
        }
        if ($('#beta-signifier-page-color-background-picker').length > 0) {
          $('#beta-signifier-page-color-background-picker').farbtastic('#edit-beta-signifier-page-color-background');
        }
        if ($('#beta-signifier-page-color-background-picker').length > 0) {
          $('#beta-signifier-page-color-text-picker').farbtastic('#edit-beta-signifier-page-color-text');
        }
      };
    }
  }
})(jQuery);

